# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from models import VerifyCode,UserProfile
# Register your models here.
import sys;

reload(sys);
sys.setdefaultencoding("utf8")

admin.site.register(UserProfile)
admin.site.register(VerifyCode)