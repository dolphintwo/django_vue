# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from models import UserFav, UserLeavingMessage, UserAddress


# Register your models here.
import sys;

reload(sys);
sys.setdefaultencoding("utf8")
admin.site.register(UserFav)
admin.site.register(UserAddress)
admin.site.register(UserLeavingMessage)