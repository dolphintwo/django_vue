# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from models import Goods, GoodsCategory, GoodsImage, GoodsCategoryBrand, Banner
from models import HotSearchWords
from models import IndexAd

# Register your models here.
import sys;

reload(sys);
sys.setdefaultencoding("utf8")

class GoodsAdmin(admin.ModelAdmin):
    list_display=('category','goods_sn','name','click_num','sold_num','fav_num','goods_num')
    search_fields = ('name',)

class GoodsCategoryAdmin(admin.ModelAdmin):
    list_display=('name','code','desc','category_type')

admin.site.register(Goods,GoodsAdmin)
admin.site.register(GoodsCategory,GoodsCategoryAdmin)
admin.site.register(Banner)
admin.site.register(GoodsCategoryBrand)
admin.site.register(HotSearchWords)
admin.site.register(IndexAd)